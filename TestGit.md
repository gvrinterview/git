# Questions

1. How can you check your current git version?

- git --v
- git --version
- git --option
- git --current

2. What command lets you create a connection between a local and remote repository?

- git remote add new
- git remote add origin
- git remote new origin
- git remote origin

3. Describe what these Git commands do to the commit history:

```bash
git reset --hard HEAD~5
git merge --squash HEAD@{1}
```

- They reset the HEAD to the fifth commit in the repo, then merge to the master branch.
- The current branch's HEAD is reset back five commits, then prior commits are squashed into a single commit.
- They delete the last five commits.
- They merge the last five commits into a new branch.

4. Which of the following is true you when you use the following command?

`git add -A`

- All new and updated files are staged
- Files are staged in alphabetical order.
- All new files are staged
- Only updated files are staged

5. What will the following command print to the Terminal?
`git remote -v`

- A list of remote repositories and their URLs
- The current git version you're running
- An inline editor for modifying remote repositories
- The last 5 git versions you've installed

6. What does the following command do to the git repository?
`git reset --soft HEAD^`

- It deletes all previous commits and reset the repository history back to its initial state.
- It resets the working branch to the first commit.
- It keeps the HEAD at the current commit, but clears all previous commits.
- It sets HEAD to the previous commit and leaves changes from the undone commit in the stage/index.

7. What option can you use to apply git configurations across your entire git environment?

- `--all`
- `--master`
- `--global`
- `--update`

8. How can you display a list of files added or modified in a specific commit?

- Find the commit in the remote repository, as that's the only place that kind of information is stored.
- Use the `diff-tree` command with the commit hash.
- Run `git commit --info` with the commit hash.
- Access the commit stash data with `git stash`.

9. Where are files stored before they are committed to the local repository?

- Saved files
- git documents
- Staging area
- git cache

10. What commands would you use to force an overwrite of your local files with the master branch?

- ```bash
  git pull --all
  git reset --hard origin/master
  ```
- ```bash
  git pull -u origin master
  git reset --hard master
  ```
- ```bash
  git pull origin master
  git reset --hard origin/myCurrentBranch
  ```
- ```bash
  git fetch --all
  git reset --hard origin/master
  ```
- The command `pull` is `fetch` followed by either `merge` or `rebase` (in this case, `merge`). We don't want to merge. Merge would be an action to our **repository**. We just want to overwrite our **local files**.

11. Which statement is true when you use the git add -A command?

- Only new files in the working directory are staged to the index.
- All new and updated files from the working directory are staged to the index.
- All files in the working directory are staged to the index in alphabetical order.
- Only updated files in the working directory are staged to the index.

12. Your team lead needs a list of all commits that will be moved before you perform a rebase. Which command can you use to access that information?

- git rebase -log
- git rebase -i
- git rebase -verbose
- git rebase -all

13. In a situation where you have several commits for a single task, what is the most efficient way to restructure your commit history?

- Cherry pick the related commits to another branch.
- Delete the task commits and recommit with a new message.
- Squash the related commits together into a single coherent commit.
- Stash the related commits under a new hash.

14. After pushing commits to the remote repository for the first time using the command below, what shorthand command can you use in future?

```bash
git push -u origin master
```

- git push master
- git push origin
- Same as before, git push -u origin master
- git push

15. What command would let you modify your previous commit?

- --fix
- --quickfix
- --modify
- --amend
